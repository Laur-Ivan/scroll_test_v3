﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Thumbnail:MonoBehaviour {

	private ThumbnailVO _thumbnailVO;

	private ThumbnailManager thumbnailManager;
	
	[SerializeField] private Image image;
	[SerializeField] private Text childText;
	
	void OnEnable()
	{
		_thumbnailVO = new ThumbnailVO();
		thumbnailManager = FindObjectOfType<ThumbnailManager>();
	}
	
	void ScrollCellIndex (int idx) 
	{
		string name = "Cell " + idx.ToString ();

		thumbnailVO = thumbnailManager.GetThumbnailFromList(idx);
		
		gameObject.name = name;
	}
	
	public ThumbnailVO thumbnailVO 
	{
		get 
		{
			return _thumbnailVO;
		}
		set 
		{
			_thumbnailVO = value;
			
			image.sprite = SpriteManager.instance.GetSprite(_thumbnailVO.id);
			childText.text = _thumbnailVO.id;
		}
	}

	public void OnClick() {
		Debug.Log("Thumbnail clicked: "+_thumbnailVO.id);
	}
}